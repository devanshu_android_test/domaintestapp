package com.domain.test.android.app.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.domain.test.android.app.common.StorageKey;
import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.intf.IItemClickListener;
import com.domain.test.android.app.intf.IRequestListener;
import com.domain.test.android.app.logger.Logger;
import com.domain.test.android.app.manager.PreferenceManager;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public abstract class AbstractBaseActivity extends AppCompatActivity implements IBaseView.View, View.OnClickListener, View.OnTouchListener, IItemClickListener, IRequestListener {

    protected final String TAG = this.getClass().getSimpleName();

    protected Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setWindowAnimations(0);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mContext = this;
    }

    @Override
    public void onBackPressed() {
        if(PreferenceManager.getBoolean(StorageKey.TabletDevice)) {
            finish();
        } else {
            int mFragmentCount = getSupportFragmentManager().getBackStackEntryCount();
            if (mFragmentCount > 1) {
                getSupportFragmentManager().popBackStack();
            } else if (mFragmentCount == 1) {
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onClick(View mView) {
        Logger.debug(TAG, "Click on view :" + mView.getClass().getSimpleName());
    }

    @Override
    public boolean onTouch(View mView, MotionEvent mMotionEvent) {
        return true;
    }

    @Override
    public void onItemClick(View mView, Object mTagObject) {
        Logger.debug(TAG, "Click on item :" + mView.getClass().getSimpleName());
    }

    @Override
    public void onError(Object mErrorResponse) {
        Logger.debug(TAG, "Web service response error");
    }

    @Override
    public void onSuccess(Object mSuccessResponse) {
        Logger.debug(TAG, "Web service response successful");
    }

    @Override
    public abstract void onInitialiseView(IBaseView.View mView);

    protected void loadFragment(int mFragmentContainer, String mFragmentClassName) {
        loadFragment(mFragmentContainer, mFragmentClassName, null);
    }

    protected void loadFragment(int mFragmentContainer, String mFragmentClassName, Bundle mBundle) {
        Fragment mFragment        = findFragmentById(mFragmentContainer);
        Fragment mRequestFragment = Fragment.instantiate(this, mFragmentClassName);
        if(null != mBundle)
            mRequestFragment.setArguments(mBundle);

        manageFragment(mFragmentContainer, mRequestFragment, mFragmentClassName,
                mFragment != null ? mFragment.getClass().getName() : mFragmentClassName);
    }

    protected Fragment findFragmentById(int mFragmentContainer) {
        return getSupportFragmentManager().findFragmentById(mFragmentContainer);
    }

    protected Fragment findFragmentByTag(String mCurrentFragment) {
        return getSupportFragmentManager().findFragmentByTag(mCurrentFragment);
    }

    private void manageFragment(int mFragmentContainer, Fragment mFragment, String mTag, String mCurrentFragment) {
        FragmentManager mManager         = getSupportFragmentManager();
        FragmentTransaction mTransaction = mManager.beginTransaction();

        if (findFragmentByTag(mCurrentFragment) != null) {
            mTransaction.hide(findFragmentByTag(mCurrentFragment));
        }

        mTransaction.add(mFragmentContainer, mFragment, mTag);
        mTransaction.addToBackStack(mTag);
        mTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        mTransaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        mTransaction.commitAllowingStateLoss();
        mManager.executePendingTransactions();
    }
}
