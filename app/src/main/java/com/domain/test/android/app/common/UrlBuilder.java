package com.domain.test.android.app.common;

import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class UrlBuilder {

    private String mUrl;
    private HashMap<String, Object> mRequestParam;

    public UrlBuilder(String mUrl) {
        this.mUrl = mUrl;

        this.mRequestParam = new HashMap<>();
    }

    public UrlBuilder addParameter(String mKey, Object mValue) {
        mRequestParam.put(mKey, mValue);

        return this;
    }

    public String build() {
        StringBuilder mQueryString = new StringBuilder();

        if (null != mRequestParam && !mRequestParam.isEmpty()) {
            Iterator<String> iterator = mRequestParam.keySet().iterator();
            while (iterator.hasNext()) {
                String key = iterator.next();
                mQueryString.append(key + "=" + mRequestParam.get(key));
                if(iterator.hasNext())
                    mQueryString.append("&");
            }
        }

        return mUrl + "?" + mQueryString;
    }
}
