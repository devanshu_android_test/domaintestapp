package com.domain.test.android.app.presenter.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.domain.test.android.app.base.AbstractBasePresenter;
import com.domain.test.android.app.common.Constants;
import com.domain.test.android.app.contract.fragment.IPropertyList;
import com.domain.test.android.app.contract.view.IBaseListView;
import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.controller.broadcast.BroadcastController;
import com.domain.test.android.app.model.Listing;
import com.domain.test.android.app.view.activity.DashboardActivity;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class PropertyListPresenter extends AbstractBasePresenter<IPropertyList.View> implements IPropertyList.Presenter {

    public static class Layout {
        public static final int Grid  = 0;
        public static final int Liner = 1;
    }

    public PropertyListPresenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public IPropertyList.View getGhostView() {
        return new IPropertyList.View() {
            @Override
            public void onInitialiseListener(IBaseView.View mView) {
            }

            @Override
            public void onInitialiseAdapter(IBaseListView.View mView) {
            }

            @Override
            public void onRequestListData(IBaseListView.View mView) {
            }

            @Override
            public void onRefreshListAdapter(IBaseListView.View mView) {
            }

            @Override
            public void onInitialiseLinearLayout(IBaseListView.View mView) {
            }

            @Override
            public void onInitialiseGridLayout(IBaseListView.View mView, int mGridItems) {
            }

            @Override
            public void onSetError(IBaseListView.View mView, String mErrorMessage) {
            }

            @Override
            public void onInitialiseView(IBaseView.View mView) {
            }
        };
    }

    @Override
    public void attachView(IPropertyList.View mView) {
        super.attachView(mView);
        getView().onInitialiseView(mView);
        getView().onInitialiseListener(mView);
    }

    @Override
    public void initialiseListView(int mLayoutType, int mGridItems) {
        if(Layout.Liner == mLayoutType) {
            getView().onInitialiseLinearLayout(getView());
        } else {
            getView().onInitialiseGridLayout(getView(), mGridItems);
        }
        getView().onInitialiseAdapter(getView());
    }

    @Override
    public void requestListData() {
        getView().onRequestListData(getView());
    }

    @Override
    public void refreshListAdapter() {
        getView().onRefreshListAdapter(getView());
    }

    @Override
    public void showFragment(Activity mActivity, Listing mListing) {
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(Constants.IntentKey.Data, mListing);
        ((DashboardActivity) mActivity).showFragment(mBundle);
    }

    @Override
    public void sendBroadcast(Listing mListing) {
        Intent mBroadcastIntent = new Intent(Constants.Broadcast.ListingDetails);
        mBroadcastIntent.putExtra(Constants.IntentKey.Data, mListing);
        BroadcastController.sendBroadcast(mContext, mBroadcastIntent);
    }

    @Override
    public void setError(String mErrorMessage) {
        getView().onSetError(getView(), mErrorMessage);
    }
}
