package com.domain.test.android.app.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;

import com.domain.test.android.app.R;
import com.domain.test.android.app.base.AbstractBaseActivity;
import com.domain.test.android.app.contract.activity.IDashboard;
import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.presenter.activity.DashboardPresenter;
import com.domain.test.android.app.view.fragment.PropertyDetailsFragment;
import com.domain.test.android.app.view.fragment.PropertyListFragment;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class DashboardActivity extends AbstractBaseActivity implements IDashboard.View {

    private FrameLayout mListFragment;
    private FrameLayout mDetailsFragment;

    private IDashboard.Presenter mPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dashboard);

        mPresenter = new DashboardPresenter(mContext);
        mPresenter.attachView(this);

        mPresenter.setDeviceType(null != findViewById(R.id.detail_fragment));

        if(mPresenter.isDeviceTablet()) {
            mPresenter.setFragmentWidth();
        }

        if(null == savedInstanceState) {
            mPresenter.loadFragment();
        }
    }

    @Override
    public void onLoadFragments(IDashboard.View nView, boolean isTablet) {
        if(isTablet) {
            loadFragment(mDetailsFragment.getId(), PropertyDetailsFragment.class.getName());
        }
        loadFragment(mListFragment.getId(), PropertyListFragment.class.getName());
    }

    @Override
    public void onInitialiseView(IBaseView.View mView) {
        mListFragment    = findViewById(R.id.list_fragment);
        mDetailsFragment = findViewById(R.id.detail_fragment);
    }

    @Override
    public void onSetFragmentWidth(IDashboard.View nView, int mWidth) {
        mListFragment.getLayoutParams().width    = mWidth;
        mDetailsFragment.getLayoutParams().width = mWidth;
    }

    public void showFragment(Bundle mBundle) {
        loadFragment(mListFragment.getId(), PropertyDetailsFragment.class.getName(), mBundle);
    }
}
