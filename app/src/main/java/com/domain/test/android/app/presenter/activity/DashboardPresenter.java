package com.domain.test.android.app.presenter.activity;

import android.content.Context;

import com.domain.test.android.app.base.AbstractBasePresenter;
import com.domain.test.android.app.common.StorageKey;
import com.domain.test.android.app.common.Utility;
import com.domain.test.android.app.contract.activity.IDashboard;
import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.manager.PreferenceManager;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class DashboardPresenter extends AbstractBasePresenter<IDashboard.View> implements IDashboard.Presenter {

    public DashboardPresenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public IDashboard.View getGhostView() {
        return new IDashboard.View() {
            @Override
            public void onLoadFragments(IDashboard.View nView, boolean isTablet) {
            }

            @Override
            public void onSetFragmentWidth(IDashboard.View nView, int mWidth) {
            }

            @Override
            public void onInitialiseView(IBaseView.View mView) {
            }
        };
    }

    @Override
    public void attachView(IDashboard.View mView) {
        super.attachView(mView);
        getView().onInitialiseView(mView);
    }

    @Override
    public void setFragmentWidth() {
        getView().onSetFragmentWidth(getView(), (Utility.getDeviceWidth(mContext)/2));
    }

    @Override
    public void loadFragment() {
        getView().onLoadFragments(getView(), isDeviceTablet());
    }

    @Override
    public void setDeviceType(boolean isTablet) {
        PreferenceManager.setBoolean(StorageKey.TabletDevice, isTablet);
    }

    @Override
    public boolean isDeviceTablet() {
        return PreferenceManager.getBoolean(StorageKey.TabletDevice);
    }
}
