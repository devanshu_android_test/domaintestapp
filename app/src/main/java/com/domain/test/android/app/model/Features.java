package com.domain.test.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class Features implements Serializable {

    @SerializedName("Value")
    private int mValue;
    @SerializedName("Name")
    private String mName;

    public int getValue() {
        return mValue;
    }

    public void setValue(int mValue) {
        this.mValue = mValue;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
