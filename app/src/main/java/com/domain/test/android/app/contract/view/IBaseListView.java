package com.domain.test.android.app.contract.view;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public interface IBaseListView {
    interface View extends IBaseView.View {
        void onInitialiseListener(IBaseView.View mView);
        void onInitialiseAdapter(View mView);
        void onRequestListData(View mView);
        void onRefreshListAdapter(View mView);
        void onInitialiseLinearLayout(View mView);
        void onInitialiseGridLayout(View mView, int mGridItems);
        void onSetError(View mView, String mErrorMessage);
    }

    interface Presenter {
        void initialiseListView(int mLayoutType, int mGridItems);
        void requestListData();
        void refreshListAdapter();
        void setError(String mErrorMessage);
    }
}
