package com.domain.test.android.app.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.domain.test.android.app.R;
import com.domain.test.android.app.controller.api.VolleyRequestHelper;

import java.util.ArrayList;

/**
 * Created by Devanshu.Verma on 30/1/18.
 */
public class PropertyImagePagerAdapter extends PagerAdapter {

    private Context mContext;

    private View mItemView;

    private NetworkImageView mPropertyImage;

    private ArrayList<String> mPropertyImageList;

    public PropertyImagePagerAdapter(Context mContext, ArrayList<String> mPropertyImageList) {
        this.mContext           = mContext;
        this.mPropertyImageList = mPropertyImageList;
    }

    @Override
    public int getCount() {
        return mPropertyImageList != null ? mPropertyImageList.size() : 0;
    }

    @Override
    public boolean isViewFromObject(View mView, Object mObject) {
        return mView == ((RelativeLayout) mObject);
    }

    @Override
    public Object instantiateItem(ViewGroup mContainer, int mPosition) {
        mItemView = LayoutInflater.from(mContext).inflate(R.layout.list_item_property_image, mContainer, false);

        initialiseView(mItemView);

        updateView(mPropertyImageList.get(mPosition));

        mContainer.addView(mItemView);

        return mItemView;
    }

    @Override
    public void destroyItem(ViewGroup mContainer, int mPosition, Object mObject) {
        mContainer.removeView((RelativeLayout) mObject);
    }

    private void initialiseView(View mItemView) {
        mPropertyImage = mItemView.findViewById(R.id.property_hero_image);
    }

    private void updateView(final String mPropertyImageUrl) {
        ImageLoader mImageLoader = VolleyRequestHelper.getInstance(mContext).getImageLoader();
        mPropertyImage.setImageUrl(mPropertyImageUrl, mImageLoader);
    }
}
