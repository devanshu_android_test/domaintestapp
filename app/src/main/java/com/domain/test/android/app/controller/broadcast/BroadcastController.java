package com.domain.test.android.app.controller.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Created by Devanshu.Verma on 30/1/18.
 */
public class BroadcastController {

    public static void sendBroadcast(Context mContext, Intent mIntent) {
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(mIntent);
    }

    public static void unregisterReceiver(Context mContext, BroadcastReceiver mReceiver) {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReceiver);
    }

    public static void registerReceiver(Context mContext, String mFilter, BroadcastReceiver mReceiver) {
        IntentFilter mIntentFilter = new IntentFilter(mFilter);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReceiver, mIntentFilter);
    }
}