package com.domain.test.android.app.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.domain.test.android.app.R;
import com.domain.test.android.app.adapter.PropertyListAdapter;
import com.domain.test.android.app.base.AbstractBaseFragment;
import com.domain.test.android.app.common.StorageKey;
import com.domain.test.android.app.contract.fragment.IPropertyList;
import com.domain.test.android.app.contract.view.IBaseListView;
import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.controller.api.ApiController;
import com.domain.test.android.app.intf.IPullToRefresh;
import com.domain.test.android.app.intf.IRecyclerScrollListener;
import com.domain.test.android.app.manager.PreferenceManager;
import com.domain.test.android.app.manager.RequestManager;
import com.domain.test.android.app.model.Listing;
import com.domain.test.android.app.model.dto.SearchResponse;
import com.domain.test.android.app.presenter.fragment.PropertyListPresenter;

import java.util.ArrayList;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class PropertyListFragment extends AbstractBaseFragment implements IPropertyList.View, IRecyclerScrollListener {

    private ArrayList<Listing> mPropertyListing;

    private IPropertyList.Presenter mPresenter;

    private PropertyListAdapter mPropertyListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mPresenter = new PropertyListPresenter(mContext);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mRootView = inflater.inflate(R.layout.fragment_property_list, container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.attachView(this);
        mPresenter.initialiseListView(PropertyListPresenter.Layout.Liner, -1);
        if(null == mPropertyListing || mPropertyListing.size() <=0)
            mPresenter.requestListData();
        else
            mPresenter.refreshListAdapter();
    }

    @Override
    public void onInitialiseView(IBaseView.View mView) {
        mSwipeContainer   = mRootView.findViewById(R.id.swipe_container);
        mRecyclerListView = mRootView.findViewById(R.id.recycler_list_view);
    }

    @Override
    public void onInitialiseAdapter(IBaseListView.View mView) {
        mPropertyListAdapter = new PropertyListAdapter(mContext, mRecyclerListView, mPropertyListing);
        mPropertyListAdapter.setOnScrollListener(this);
        mPropertyListAdapter.setOnItemClickListener(this);
        mRecyclerListView.setAdapter(mPropertyListAdapter);
    }

    @Override
    public void onInitialiseListener(IBaseView.View mView) {
        mSwipeContainer.setOnRefreshListener(mOnRefreshListener);
        mPullToRefresh = new IPullToRefresh() {
            @Override
            public void onPullToRefresh() {
                mPresenter.requestListData();
                mSwipeContainer.setRefreshing(false);
            }
        };
    }

    @Override
    public void onInitialiseGridLayout(IBaseListView.View mView, int mGridItems) {
        mRecyclerLayoutManager = new GridLayoutManager(mContext, mGridItems);
        mRecyclerListView.setLayoutManager(mRecyclerLayoutManager);
    }

    @Override
    public void onInitialiseLinearLayout(IBaseListView.View mView) {
        mRecyclerLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerListView.setLayoutManager(mRecyclerLayoutManager);
    }

    @Override
    public void onSetError(IBaseListView.View mView, String mErrorMessage) {
        Snackbar mSnackBar = Snackbar
                .make(mSwipeContainer, mErrorMessage, Snackbar.LENGTH_SHORT);
        mSnackBar.setActionTextColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
        View mSnackBarView = mSnackBar.getView();
        TextView mSnackBarText = mSnackBarView.findViewById(android.support.design.R.id.snackbar_text);
        mSnackBarText.setTextColor(ContextCompat.getColor(mContext, R.color.color_white));
        mSnackBarView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_snack_bar));
        mSnackBar.show();
    }

    @Override
    public void onRequestListData(IBaseListView.View mView) {
        final String mEndpoint = RequestManager.getSearchRequest("Bondi", "buy", "NSW", "2026");
        new ApiController<SearchResponse>(mContext, this).get(mEndpoint, SearchResponse.class, "");
    }

    @Override
    public void onRefreshListAdapter(IBaseListView.View mView) {
        stopProgressBar(R.id.progress_bar);
        int mPosition = ((LinearLayoutManager) mRecyclerListView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        mPropertyListAdapter.updateAdapterList(mPropertyListing);
        mRecyclerListView.scrollToPosition(mPosition);
    }

    @Override
    public void onError(Object mErrorResponse) {
        super.onError(mErrorResponse);
        stopProgressBar(R.id.progress_bar);
        ApiController.ErrorResponse mError = (ApiController.ErrorResponse) mErrorResponse;
        mPresenter.setError(mError.getResponseMessage());
    }

    @Override
    public void onSuccess(Object mSuccessResponse) {
        super.onSuccess(mSuccessResponse);
        SearchResponse mSearchResponse = (SearchResponse) mSuccessResponse;
        mPropertyListing = mSearchResponse.getListingResults().getListings();
        if(PreferenceManager.getBoolean(StorageKey.TabletDevice))
            mPresenter.sendBroadcast(mPropertyListing.get(0));
        mPresenter.refreshListAdapter();
    }

    @Override
    public void onItemClick(View mView, Object mTagObject) {
        super.onItemClick(mView, mTagObject);
        switch (mView.getId()) {
            case R.id.shortlisted:
                mPropertyListing.get((Integer) mTagObject).setShortlisted(!mPropertyListing.get((Integer) mTagObject).isShortlisted());
                 mPropertyListAdapter.notifyItemChanged((Integer) mTagObject);
                break;
            default:
                if(PreferenceManager.getBoolean(StorageKey.TabletDevice))
                    mPresenter.sendBroadcast(mPropertyListing.get((Integer) mTagObject));
                else
                    mPresenter.showFragment(getActivity(), mPropertyListing.get((Integer) mTagObject));
                break;
        }
    }

    @Override
    public void onListScrolledUp() {
        mPropertyListAdapter.setScrollAnimation(true);
    }

    @Override
    public void onListScrolledDown() {
        mPropertyListAdapter.setScrollAnimation(false);
    }
}
