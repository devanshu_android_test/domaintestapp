package com.domain.test.android.app.common;

import com.google.gson.Gson;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class RequestBuilder {

    public static Object getObjectFromJSON(String mJsonString, Class<?> mClassType) {
        return new Gson().fromJson(mJsonString, mClassType);
    }
}
