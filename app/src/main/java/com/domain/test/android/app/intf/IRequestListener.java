package com.domain.test.android.app.intf;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public interface IRequestListener {
    void onError(Object mErrorResponse);
    void onSuccess(Object mSuccessResponse);
}
