package com.domain.test.android.app.manager;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class UrlManager {

    private static class Server {
        public static final String Name = "https://rest.domain.com.au/";
    }

    private static class Service {
        public static String Search = Server.Name + "searchservice.svc/";
    }

    public static class MapSearch {
        public static final String EndPoint = Service.Search + "mapsearch";
    }
}
