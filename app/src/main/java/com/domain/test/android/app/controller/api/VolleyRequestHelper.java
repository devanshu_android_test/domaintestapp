package com.domain.test.android.app.controller.api;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.domain.test.android.app.R;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class VolleyRequestHelper {

    private static VolleyRequestHelper mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static Context mContext;

    private VolleyRequestHelper(Context mContext) {
        VolleyRequestHelper.mContext = mContext;
        initVolleyRequestQueue(mContext);
    }

    public static synchronized VolleyRequestHelper getInstance(Context mContext) {
        if (mInstance == null) {
            mInstance = new VolleyRequestHelper(mContext);
        }
        return mInstance;
    }

    public void initVolleyRequestQueue(Context mContext) {
        mRequestQueue = Volley.newRequestQueue(mContext);
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? mContext.getResources().getString(R.string.app_name) : tag);
        mRequestQueue.add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public ImageLoader getImageLoader() {
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(mRequestQueue, new LruBitmapCache());
        }
        return mImageLoader;
    }
}
