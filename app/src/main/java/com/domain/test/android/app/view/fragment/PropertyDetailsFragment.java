package com.domain.test.android.app.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.domain.test.android.app.R;
import com.domain.test.android.app.adapter.PropertyImagePagerAdapter;
import com.domain.test.android.app.adapter.viewholder.PropertyFeatureViewHolder;
import com.domain.test.android.app.base.AbstractBaseFragment;
import com.domain.test.android.app.common.Constants;
import com.domain.test.android.app.common.Utility;
import com.domain.test.android.app.contract.fragment.IPropertyDetails;
import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.controller.api.VolleyRequestHelper;
import com.domain.test.android.app.model.Features;
import com.domain.test.android.app.model.ImageDimensions;
import com.domain.test.android.app.model.Listing;
import com.domain.test.android.app.presenter.fragment.PropertyDetailsPresenter;

import java.util.ArrayList;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class PropertyDetailsFragment extends AbstractBaseFragment implements IPropertyDetails.View {

    private final class LayoutMargin {
        public static final int Top    = 0;
        public static final int Left   = 15;
        public static final int Right  = 15;
        public static final int Bottom = 0;
    }

    private int mIndicatorCount;

    private ViewPager mOfferImagePager;

    private LinearLayout mPagerIndicator;

    private ImageView[] mIndicators;

    private PropertyImagePagerAdapter mImagePagerAdapter;

    private NetworkImageView mAgencyLogo;
    private NetworkImageView mAgencyContact;

    private Listing mProperty;

    private TextView mCarPark;
    private TextView mBedrooms;
    private TextView mBathrooms;

    private TextView mArea;
    private TextView mSuburb;
    private TextView mPropertyType;

    private TextView mAuctionDate;
    private TextView mInspectionDate;

    private TextView mHeadline;
    private TextView mDescription;

    private TextView mPropertyLabel;
    private TextView mPropertyPrice;
    private TextView mPropertyAddress;

    private ImageView mShortlisted;

    private LinearLayout   mParentLayout;
    private RelativeLayout mAgencyLayout;
    private LinearLayout   mFeatureLayout;

    private CardView mDatesCard;
    private CardView mFeatureCard;

    private PropertyFeatureViewHolder mPropertyFeatureViewHolder;

    private IPropertyDetails.Presenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mPresenter = new PropertyDetailsPresenter(mContext);
        mPresenter.registerReceiver(Constants.Broadcast.ListingDetails, mUpdateViewReceiver);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mRootView = inflater.inflate(R.layout.fragment_property_details, container, false);
        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter.attachView(this);
        Bundle mBundle = getArguments();
        if(null != mBundle)
            mProperty = (Listing) mBundle.getSerializable(Constants.IntentKey.Data);
        if(null != mProperty)
            mPresenter.dataReceived(mProperty);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.unregisterReceiver(mUpdateViewReceiver);
    }

    @Override
    public void onInitialiseView(IBaseView.View mView) {

        mPagerIndicator  = mRootView.findViewById(R.id.pager_indicator);
        mOfferImagePager = mRootView.findViewById(R.id.property_image_pager);

        mShortlisted = mRootView.findViewById(R.id.shortlisted);

        mAgencyLogo    = mRootView.findViewById(R.id.agency_logo);
        mAgencyContact = mRootView.findViewById(R.id.agency_contact);

        mArea   = mRootView.findViewById(R.id.area);
        mSuburb = mRootView.findViewById(R.id.suburb);

        mCarPark   = mRootView.findViewById(R.id.car_park);
        mBedrooms  = mRootView.findViewById(R.id.bedrooms);
        mBathrooms = mRootView.findViewById(R.id.bathrooms);

        mPropertyLabel   = mRootView.findViewById(R.id.property_label);
        mPropertyPrice   = mRootView.findViewById(R.id.property_price);
        mPropertyAddress = mRootView.findViewById(R.id.property_address);

        mPropertyType  = mRootView.findViewById(R.id.property_type);

        mAuctionDate    = mRootView.findViewById(R.id.auction_date);
        mInspectionDate = mRootView.findViewById(R.id.inspection_date);

        mHeadline    = mRootView.findViewById(R.id.headline);
        mDescription = mRootView.findViewById(R.id.description);

        mParentLayout  = mRootView.findViewById(R.id.parent_layout);
        mAgencyLayout  = mRootView.findViewById(R.id.agency_layout);
        mFeatureLayout = mRootView.findViewById(R.id.features_layout);

        mDatesCard   = mRootView.findViewById(R.id.property_dates_card);
        mFeatureCard = mRootView.findViewById(R.id.property_features_card);
    }

    @Override
    public void onSetLayoutWidth(IPropertyDetails.View mView, int mScreenHeight, int mScreenWidth) {
        ImageDimensions mImageDimensions = Utility.getImageDimensions(mScreenWidth, Constants.AspectRatio.HeroPager.Width, Constants.AspectRatio.HeroPager.Height, Constants.AspectRatio.HeroPager.ImageOccupancy);

        mOfferImagePager.getLayoutParams().width  = mImageDimensions.getWidth();
        mOfferImagePager.getLayoutParams().height = mImageDimensions.getHeight();

        mImageDimensions = Utility.getImageDimensions(mScreenWidth, Constants.AspectRatio.AgencyLogo.Width, Constants.AspectRatio.AgencyLogo.Height, Constants.AspectRatio.AgencyLogo.ImageOccupancy);

        mAgencyLogo.getLayoutParams().width  = mImageDimensions.getWidth();
        mAgencyLogo.getLayoutParams().height = mImageDimensions.getHeight();

        mImageDimensions = Utility.getImageDimensions(mScreenWidth, Constants.AspectRatio.AgencyContact.Width, Constants.AspectRatio.AgencyContact.Height, Constants.AspectRatio.AgencyContact.ImageOccupancy);

        mAgencyContact.getLayoutParams().width  = mImageDimensions.getWidth();
        mAgencyContact.getLayoutParams().height = mImageDimensions.getHeight();
    }

    @Override
    public void onUpdateView(IPropertyDetails.View mView, Listing mProperty) {
        mParentLayout.setVisibility(View.VISIBLE);

        ImageLoader mImageLoader = VolleyRequestHelper.getInstance(mContext).getImageLoader();

        onInitialisePager(mProperty.getImageUrls());

        setupPageIndicators();

        if(mProperty.isShortlisted())
            mShortlisted.setImageResource(android.R.drawable.star_big_on);
        else
            mShortlisted.setImageResource(android.R.drawable.star_big_off);

        mAgencyLogo.setImageUrl(mProperty.getAgencyLogoUrl(), mImageLoader);
        mAgencyContact.setImageUrl(mProperty.getAgencyContactPhoto(), mImageLoader);
        mAgencyLayout.setBackgroundColor(Color.parseColor(mProperty.getAgencyColour()));

        mArea.setText(mProperty.getArea());
        mSuburb.setText(mProperty.getSuburb());

        mCarPark.setText(mProperty.getCarSpaces() + mContext.getResources().getString(R.string.car_park));
        mBedrooms.setText(mProperty.getBedrooms() + mContext.getResources().getString(R.string.bedroom));
        mBathrooms.setText(mProperty.getBathrooms() + mContext.getResources().getString(R.string.bathroom));

        if(null != mProperty.getStatusLabel() && !TextUtils.isEmpty(mProperty.getStatusLabel()))
            mPropertyLabel.setText(mProperty.getStatusLabel());
        else
            mPropertyLabel.setVisibility(View.GONE);

        mPropertyPrice.setText(mProperty.getDisplayPrice());
        mPropertyAddress.setText(mProperty.getDisplayableAddress());

        mPropertyType.setText(mProperty.getPropertyType());

        if((null == mProperty.getAuctionDate() || TextUtils.isEmpty(mProperty.getAuctionDate())) &&
                (null == mProperty.getInspectionDate() || TextUtils.isEmpty(mProperty.getInspectionDate()))) {
            mDatesCard.setVisibility(View.GONE);
        } else {
            mDatesCard.setVisibility(View.VISIBLE);
            if((null != mProperty.getAuctionDate() && !TextUtils.isEmpty(mProperty.getAuctionDate())))
                mAuctionDate.setText(mPresenter.getDateFromString(mProperty.getAuctionDate()));
            if((null != mProperty.getInspectionDate() && !TextUtils.isEmpty(mProperty.getInspectionDate())))
                mInspectionDate.setText(mPresenter.getDateFromString(mProperty.getInspectionDate()));
        }

        mHeadline.setText(Html.fromHtml(mProperty.getHeadline()));
        mDescription.setText(Html.fromHtml(mProperty.getDescription()));

        if(null != mProperty.getFeatures() && mProperty.getFeatures().size() > 0) {
            if (mFeatureLayout.getTag() == null) {
                mFeatureCard.setVisibility(View.VISIBLE);
                for (Features mFeatures : mProperty.getFeatures()) {
                    mFeatureLayout.addView(getPropertyFeatureRow(mFeatures));
                }
                mFeatureLayout.setTag(mPropertyFeatureViewHolder);
            }
        } else {
            mFeatureCard.setVisibility(View.GONE);
        }

        this.mProperty = mProperty;
    }

    private void onInitialisePager(ArrayList<String> mPropertyImageList) {
        mImagePagerAdapter = new PropertyImagePagerAdapter(mContext, mPropertyImageList);

        mOfferImagePager.setAdapter(mImagePagerAdapter);
        mOfferImagePager.setOffscreenPageLimit(mImagePagerAdapter.getCount());
        mOfferImagePager.setCurrentItem(0);
        mOfferImagePager.addOnPageChangeListener(mPageChangeListener);
    }

    private void setupPageIndicators() {
        mIndicatorCount = mImagePagerAdapter.getCount();
        mIndicators = new ImageView[mIndicatorCount];

        mPagerIndicator.removeAllViews();

        for (int i = 0; i < mIndicatorCount; i++) {
            mIndicators[i] = new ImageView(mContext);
            mIndicators[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.unselected_item_dot));

            LinearLayout.LayoutParams mLayoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            mLayoutParams.setMargins(LayoutMargin.Left, LayoutMargin.Top, LayoutMargin.Right, LayoutMargin.Bottom);
            mPagerIndicator.addView(mIndicators[i], mLayoutParams);
        }
        if(mIndicators.length > 0)
            mIndicators[0].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.selected_item_dot));
    }

    private View getPropertyFeatureRow(Features mFeatures){
        View mView = LayoutInflater.from(mContext).inflate(R.layout.list_item_property_feature, null);
        mPropertyFeatureViewHolder = new PropertyFeatureViewHolder(mView);

        mPropertyFeatureViewHolder.mFeatureName.setText(mFeatures.getName());
        mPropertyFeatureViewHolder.mFeatureValue.setText(String.valueOf(mFeatures.getValue()));
        return mView;

    }

    private BroadcastReceiver mUpdateViewReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context mContext, Intent mIntent) {
            mPresenter.dataReceived((Listing) mIntent.getSerializableExtra(Constants.IntentKey.Data));
        }
    };

    private ViewPager.OnPageChangeListener mPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }

        @Override
        public void onPageSelected(int position) {
            for (int i = 0; i < mIndicatorCount; i++) {
                mIndicators[i].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.unselected_item_dot));
            }
            mIndicators[position].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.selected_item_dot));
        }
    };

}
