package com.domain.test.android.app.contract.fragment;

import android.app.Activity;

import com.domain.test.android.app.contract.view.IBaseListView;
import com.domain.test.android.app.model.Listing;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public interface IPropertyList {
    interface View extends IBaseListView.View {
    }

    interface Presenter extends IBaseListView.Presenter {
        void attachView(View mView);
        void showFragment(Activity mActivity, Listing mListing);
        void sendBroadcast(Listing mListing);
    }
}
