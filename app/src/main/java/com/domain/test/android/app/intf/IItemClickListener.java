package com.domain.test.android.app.intf;

import android.view.View;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public interface IItemClickListener {
    void onItemClick(View mView, Object mTagObject);
}
