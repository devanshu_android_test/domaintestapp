package com.domain.test.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class ListingResult implements Serializable {

    @SerializedName("ResultsTotal")
    private int mResultsTotal;
    @SerializedName("ResultsReturned")
    private int mResultsReturned;
    @SerializedName("TopSpotAgencyID")
    private int mTopSpotAgencyID;
    @SerializedName("TopSpotListingsCount")
    private int mTopSpotListingsCount;

    @SerializedName("Listings")
    private ArrayList<Listing> mListings;

    public int getResultsTotal() {
        return mResultsTotal;
    }

    public void setResultsTotal(int mResultsTotal) {
        this.mResultsTotal = mResultsTotal;
    }

    public int getResultsReturned() {
        return mResultsReturned;
    }

    public void setResultsReturned(int mResultsReturned) {
        this.mResultsReturned = mResultsReturned;
    }

    public int getTopSpotAgencyID() {
        return mTopSpotAgencyID;
    }

    public void setTopSpotAgencyID(int mTopSpotAgencyID) {
        this.mTopSpotAgencyID = mTopSpotAgencyID;
    }

    public int getTopSpotListingsCount() {
        return mTopSpotListingsCount;
    }

    public void setTopSpotListingsCount(int mTopSpotListingsCount) {
        this.mTopSpotListingsCount = mTopSpotListingsCount;
    }

    public ArrayList<Listing> getListings() {
        return mListings;
    }

    public void setListings(ArrayList<Listing> mListings) {
        this.mListings = mListings;
    }
}
