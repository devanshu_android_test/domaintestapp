package com.domain.test.android.app.controller.api;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.domain.test.android.app.common.RequestBuilder;
import com.domain.test.android.app.controller.device.DeviceController;
import com.domain.test.android.app.intf.IRequestListener;
import com.domain.test.android.app.logger.Logger;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class ApiController<ResponseType> {

    private final String TAG = ApiController.class.getSimpleName();

    private final Context mContext;

    private final IRequestListener mRequestListener;

    private final VolleyRequestHelper mVolleyRequestHelper;

    public ApiController(Context mContext, IRequestListener mRequestListener) {
        this.mContext = mContext;

        this.mRequestListener = mRequestListener;

        this.mVolleyRequestHelper = VolleyRequestHelper.getInstance(mContext);
    }

    public void get(final String mEndpoint, final Class<ResponseType> mResponseClass, final String mTag) {
        request(Request.Method.GET, mEndpoint, mResponseClass, mTag);
    }

    private void request(final int mMethod, final String mEndpoint, final Class<ResponseType> mResponseClass, final String mTag) {
        if (DeviceController.isInternetAvailable(mContext)) {
            Request mApiRequest = new VolleyHandler<ResponseType>(mMethod, mEndpoint, mResponseClass, mSuccessListener, mFailureListener);
            mVolleyRequestHelper.addToRequestQueue(mApiRequest, mTag);
        } else {
            if (null != mRequestListener)
                mRequestListener.onError(getErrorResponse(null, "Internet not available!", -1));
        }
    }

    private ErrorResponse parseVolleyError(final VolleyError mVolleyError) {
        String mJsonFeed = null;
        ErrorResponse mErrorResponse = new ErrorResponse();
        try {
            mJsonFeed = new String(mVolleyError.networkResponse.data);
            mErrorResponse.setJsonFeed(mJsonFeed);
            mErrorResponse = (ErrorResponse) RequestBuilder.getObjectFromJSON(mErrorResponse.getJsonFeed(), ErrorResponse.class);
            return mErrorResponse;
        } catch (Exception mException) {
            mException.printStackTrace();
            return getErrorResponse(mJsonFeed, "Something went wrong", -1);
        }
    }

    private ErrorResponse getErrorResponse(String mJsonFeed, String mErrorMessage, int mErrorCode) {
        ErrorResponse mErrorResponse = new ErrorResponse();
            mErrorResponse.setJsonFeed(mJsonFeed);
            mErrorResponse.setResponseMessage(mErrorMessage);
            mErrorResponse.setResponseCode(mErrorCode);
        return mErrorResponse;
    }

    private Response.Listener mSuccessListener = new Response.Listener<ResponseType>() {
        @Override
        public void onResponse(ResponseType mResponseType) {
            if(null != mResponseType) {
                if(null != mRequestListener)
                    mRequestListener.onSuccess(mResponseType);
                Logger.debug(TAG, "Received success response from server");
            } else {
                if(null != mRequestListener)
                    mRequestListener.onError(mResponseType);
                Logger.debug(TAG, "Received success but null response from server");
            }
        }
    };

    private Response.ErrorListener mFailureListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError mVolleyError) {
            if(null != mRequestListener)
                mRequestListener.onError(parseVolleyError(mVolleyError));
            Logger.error(TAG, "Received error response from server");
        }
    };

    public final class ErrorResponse {

        @SerializedName("responseCode")
        private int  mResponseCode;

        @SerializedName("responseMessage")
        private String mResponseMessage;

        private String mJsonFeed;

        public String getJsonFeed() {
            return mJsonFeed;
        }

        public void setJsonFeed(String mJsonFeed) {
            this.mJsonFeed = mJsonFeed;
        }

        public int getResponseCode() {
            return mResponseCode;
        }

        public void setResponseCode(int mResponseCode) {
            this.mResponseCode = mResponseCode;
        }

        public String getResponseMessage() {
            return mResponseMessage;
        }

        public void setResponseMessage(String mResponseMessage) {
            this.mResponseMessage = mResponseMessage;
        }
    }

    private class VolleyHandler<ResponseType> extends Request<ResponseType> {

        private final String TAG = VolleyHandler.class.getSimpleName();
        private final String mRequestBody;
        private final Response.Listener<ResponseType> mListener;

        private Class<ResponseType> mResponseClass;

        private int mTimeout;
        private int mMaxRetry;
        private final int mMilliSecond = 1000;

        private final String PROTOCOL_CHARSET = "utf-8";
        private final String PROTOCOL_CONTENT_TYPE = String.format("application/json; charset=%s", PROTOCOL_CHARSET);

        public VolleyHandler(int mMethod, String url, Class<ResponseType> mResponseClass, Response.Listener<ResponseType> mListener,
                             Response.ErrorListener mErrorListener) {
            super(mMethod, url, mErrorListener);

            this.mTimeout  = 30 * mMilliSecond;
            this.mMaxRetry = 0;
            setRetryPolicy(getDefaultRetryPolicy());
            setShouldCache(false);
            this.mResponseClass = mResponseClass;
            this.mRequestBody = "";
            this.mListener = mListener;
            Logger.debug(TAG, "Request URL : " + url);
        }

        @Override
        protected Response<ResponseType> parseNetworkResponse(NetworkResponse networkResponse) {
            String mJsonString;
            try {
                mJsonString = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                Logger.debug(TAG, "Response JSON: " + mJsonString.toString());
                ResponseType mResponse = (ResponseType) RequestBuilder.getObjectFromJSON(mJsonString, mResponseClass);
                Response<ResponseType> result = Response.success(mResponse,
                        HttpHeaderParser.parseCacheHeaders(networkResponse));
                return result;
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
                return Response.error(new ParseError(e));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return Response.error(new ParseError(e));
            } catch (Exception e) {
                e.printStackTrace();
                return Response.error(new ParseError(e));
            }
        }

        @Override
        protected void deliverResponse(ResponseType response) {
            mListener.onResponse(response);
        }


        private DefaultRetryPolicy getDefaultRetryPolicy() {
            return new DefaultRetryPolicy(mTimeout, mMaxRetry, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        }

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json");
            return headers;
        }

        @Override
        public String getBodyContentType() {
            return PROTOCOL_CONTENT_TYPE;
        }

        @Override
        public byte[] getBody() {
            try {
                return mRequestBody == null ? null : mRequestBody.getBytes(PROTOCOL_CHARSET);
            } catch (UnsupportedEncodingException uee) {
                VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                        mRequestBody, PROTOCOL_CHARSET);
                return null;
            }
        }

        @Override
        public Priority getPriority() {
            return super.getPriority();
        }
    }
}