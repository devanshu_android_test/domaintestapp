package com.domain.test.android.app.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.ImageLoader;
import com.domain.test.android.app.intf.IItemClickListener;
import com.domain.test.android.app.intf.IRecyclerScrollListener;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public abstract class AbstractBaseRecyclerListAdapter <ViewHolder extends AbstractBaseItemViewHolder> extends RecyclerView.Adapter<ViewHolder> implements View.OnClickListener {

    protected final String TAG = this.getClass().getSimpleName();

    protected final int ViewPremium  = 1;
    protected final int ViewStandard = 0;

    protected boolean isScrolling;

    protected IItemClickListener      mItemClickListener;
    protected IRecyclerScrollListener mRecyclerScrollListener;

    protected Context      mContext;
    protected ImageLoader  mImageLoader;
    protected RecyclerView mRecyclerView;

    public synchronized void setScrollAnimation(boolean isScrolling){
        this.isScrolling = isScrolling;
    }

    public void setOnScrollListener(IRecyclerScrollListener mRecyclerScrollListener) {
        this.mRecyclerScrollListener = mRecyclerScrollListener;
    }

    public void setOnItemClickListener(IItemClickListener mOnItemClickListener) {
        this.mItemClickListener = mOnItemClickListener;
    }

    @Override
    public void onClick(View mView) {
        if (mItemClickListener != null)
            mItemClickListener.onItemClick(mView, mView.getTag());
    }

    public abstract Object getItem(int mPosition);

    public abstract void updateAdapterList(Object mAdapterList);

    public abstract int getItemCount();

    public abstract int getItemViewType(int mPosition);

    public abstract ViewHolder onCreateViewHolder(ViewGroup mParent, int mViewType);

    public abstract ViewHolder getViewHolder(final ViewGroup mParent, final int mViewType);

    public abstract void onBindViewHolder(final ViewHolder mViewHolder, final int mPosition);

    protected RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0) {
                if(null != mRecyclerScrollListener)
                    mRecyclerScrollListener.onListScrolledUp();
            } else {
                if(null != mRecyclerScrollListener)
                    mRecyclerScrollListener.onListScrolledDown();
            }
        }
    };
}
