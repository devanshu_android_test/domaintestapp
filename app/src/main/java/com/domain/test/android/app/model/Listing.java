package com.domain.test.android.app.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class Listing implements Serializable {

    @SerializedName("AdId")
    private long mAdId;

    @SerializedName("GroupNo")
    private int mGroupNo;
    @SerializedName("IsElite")
    private int isElite;
    @SerializedName("Bedrooms")
    private int mBedrooms;
    @SerializedName("IsBranded")
    private int isBranded;
    @SerializedName("Bathrooms")
    private int mBathrooms;
    @SerializedName("Carspaces")
    private int mCarSpaces;
    @SerializedName("GroupCount")
    private int mGroupCount;
    @SerializedName("IsPriority")
    private int isPriority;
    @SerializedName("ListingPrice")
    private int mListingPrice;
    @SerializedName("MapCertainty")
    private int mMapCertainty;
    @SerializedName("FreshnessLevel")
    private int mFreshnessLevel;
    @SerializedName("TopSpotEligible")
    private int mTopSpotEligible;
    @SerializedName("HasEnhancedVideoUrl")
    private int mHasEnhancedVideoUrl;
    @SerializedName("UnderOfferOrContract")
    private int mUnderOfferOrContract;

    @SerializedName("Latitude")
    private double mLatitude;
    @SerializedName("Longitude")
    private double mLongitude;

    @SerializedName("HomepassEnabled")
    private boolean mHomePassEnabled;
    private boolean isShortlisted;

    @SerializedName("Area")
    private String mArea;
    @SerializedName("State")
    private String mState;
    @SerializedName("Suburb")
    private String mSuburb;
    @SerializedName("Region")
    private String mRegion;
    @SerializedName("ThumbUrl")
    private String mThumbUrl;
    @SerializedName("VideoUrl")
    private String mVideoUrl;
    @SerializedName("AgencyID")
    private String mAgencyID;
    @SerializedName("Headline")
    private String mHeadline;
    @SerializedName("ListingType")
    private String mListingType;
    @SerializedName("StatusLabel")
    private String mStatusLabel;
    @SerializedName("AuctionDate")
    private String mAuctionDate;
    @SerializedName("DateUpdated")
    private String mDateUpdated;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("DisplayPrice")
    private String mDisplayPrice;
    @SerializedName("PropertyType")
    private String mPropertyType;
    @SerializedName("AgencyColour")
    private String mAgencyColour;
    @SerializedName("AgencyLogoUrl")
    private String mAgencyLogoUrl;
    @SerializedName("InspectionDate")
    private String mInspectionDate;
    @SerializedName("SecondThumbUrl")
    private String mSecondThumbUrl;
    @SerializedName("DateFirstListed")
    private String mDateFirstListed;
    @SerializedName("ListingTypeString")
    private String mListingTypeString;
    @SerializedName("DisplayableAddress")
    private String mDisplayableAddress;
    @SerializedName("AgencyContactPhoto")
    private String mAgencyContactPhoto;
    @SerializedName("TruncatedDescription")
    private String mTruncatedDescription;
    @SerializedName("RetinaDisplayThumbUrl")
    private String mRetinaDisplayThumbUrl;
    @SerializedName("SecondRetinaDisplayThumbUrl")
    private String mSecondRetinaDisplayThumbUrl;

    @SerializedName("ImageUrls")
    private ArrayList<String>   mImageUrls;
    @SerializedName("Features")
    private ArrayList<Features> mFeatures;

    public long getAdId() {
        return mAdId;
    }

    public void setAdId(long mAdId) {
        this.mAdId = mAdId;
    }

    public int getGroupNo() {
        return mGroupNo;
    }

    public void setGroupNo(int mGroupNo) {
        this.mGroupNo = mGroupNo;
    }

    public int getIsElite() {
        return isElite;
    }

    public void setIsElite(int isElite) {
        this.isElite = isElite;
    }

    public int getBedrooms() {
        return mBedrooms;
    }

    public void setBedrooms(int mBedrooms) {
        this.mBedrooms = mBedrooms;
    }

    public int getIsBranded() {
        return isBranded;
    }

    public void setIsBranded(int isBranded) {
        this.isBranded = isBranded;
    }

    public int getBathrooms() {
        return mBathrooms;
    }

    public void setBathrooms(int mBathrooms) {
        this.mBathrooms = mBathrooms;
    }

    public int getCarSpaces() {
        return mCarSpaces;
    }

    public void setCarSpaces(int mCarSpaces) {
        this.mCarSpaces = mCarSpaces;
    }

    public int getGroupCount() {
        return mGroupCount;
    }

    public void setGroupCount(int mGroupCount) {
        this.mGroupCount = mGroupCount;
    }

    public int getIsPriority() {
        return isPriority;
    }

    public void setIsPriority(int isPriority) {
        this.isPriority = isPriority;
    }

    public int getListingPrice() {
        return mListingPrice;
    }

    public void setListingPrice(int mListingPrice) {
        this.mListingPrice = mListingPrice;
    }

    public int getMapCertainty() {
        return mMapCertainty;
    }

    public void setMapCertainty(int mMapCertainty) {
        this.mMapCertainty = mMapCertainty;
    }

    public int getFreshnessLevel() {
        return mFreshnessLevel;
    }

    public void setFreshnessLevel(int mFreshnessLevel) {
        this.mFreshnessLevel = mFreshnessLevel;
    }

    public int getTopSpotEligible() {
        return mTopSpotEligible;
    }

    public void setTopSpotEligible(int mTopSpotEligible) {
        this.mTopSpotEligible = mTopSpotEligible;
    }

    public int getHasEnhancedVideoUrl() {
        return mHasEnhancedVideoUrl;
    }

    public void setHasEnhancedVideoUrl(int mHasEnhancedVideoUrl) {
        this.mHasEnhancedVideoUrl = mHasEnhancedVideoUrl;
    }

    public int getUnderOfferOrContract() {
        return mUnderOfferOrContract;
    }

    public void setUnderOfferOrContract(int mUnderOfferOrContract) {
        this.mUnderOfferOrContract = mUnderOfferOrContract;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public boolean ismHomePassEnabled() {
        return mHomePassEnabled;
    }

    public void setHomePassEnabled(boolean mHomePassEnabled) {
        this.mHomePassEnabled = mHomePassEnabled;
    }

    public boolean isShortlisted() {
        return isShortlisted;
    }

    public void setShortlisted(boolean shortlisted) {
        isShortlisted = shortlisted;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String mArea) {
        this.mArea = mArea;
    }

    public String getState() {
        return mState;
    }

    public void setState(String mState) {
        this.mState = mState;
    }

    public String getSuburb() {
        return mSuburb;
    }

    public void setSuburb(String mSuburb) {
        this.mSuburb = mSuburb;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String mRegion) {
        this.mRegion = mRegion;
    }

    public String getThumbUrl() {
        return mThumbUrl;
    }

    public void setThumbUrl(String mThumbUrl) {
        this.mThumbUrl = mThumbUrl;
    }

    public String getVideoUrl() {
        return mVideoUrl;
    }

    public void setVideoUrl(String mVideoUrl) {
        this.mVideoUrl = mVideoUrl;
    }

    public String getAgencyID() {
        return mAgencyID;
    }

    public void setAgencyID(String mAgencyID) {
        this.mAgencyID = mAgencyID;
    }

    public String getHeadline() {
        return mHeadline;
    }

    public void setHeadline(String mHeadline) {
        this.mHeadline = mHeadline;
    }

    public String getListingType() {
        return mListingType;
    }

    public void setListingType(String mListingType) {
        this.mListingType = mListingType;
    }

    public String getStatusLabel() {
        return mStatusLabel;
    }

    public void setStatusLabel(String mStatusLabel) {
        this.mStatusLabel = mStatusLabel;
    }

    public String getAuctionDate() {
        return mAuctionDate;
    }

    public void setAuctionDate(String mAuctionDate) {
        this.mAuctionDate = mAuctionDate;
    }

    public String getDateUpdated() {
        return mDateUpdated;
    }

    public void setDateUpdated(String mDateUpdated) {
        this.mDateUpdated = mDateUpdated;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getDisplayPrice() {
        return mDisplayPrice;
    }

    public void setDisplayPrice(String mDisplayPrice) {
        this.mDisplayPrice = mDisplayPrice;
    }

    public String getPropertyType() {
        return mPropertyType;
    }

    public void setPropertyType(String mPropertyType) {
        this.mPropertyType = mPropertyType;
    }

    public String getAgencyColour() {
        return mAgencyColour;
    }

    public void setAgencyColour(String mAgencyColour) {
        this.mAgencyColour = mAgencyColour;
    }

    public String getAgencyLogoUrl() {
        return mAgencyLogoUrl;
    }

    public void setAgencyLogoUrl(String mAgencyLogoUrl) {
        this.mAgencyLogoUrl = mAgencyLogoUrl;
    }

    public String getInspectionDate() {
        return mInspectionDate;
    }

    public void setInspectionDate(String mInspectionDate) {
        this.mInspectionDate = mInspectionDate;
    }

    public String getSecondThumbUrl() {
        return mSecondThumbUrl;
    }

    public void setSecondThumbUrl(String mSecondThumbUrl) {
        this.mSecondThumbUrl = mSecondThumbUrl;
    }

    public String getDateFirstListed() {
        return mDateFirstListed;
    }

    public void setDateFirstListed(String mDateFirstListed) {
        this.mDateFirstListed = mDateFirstListed;
    }

    public String getListingTypeString() {
        return mListingTypeString;
    }

    public void setListingTypeString(String mListingTypeString) {
        this.mListingTypeString = mListingTypeString;
    }

    public String getDisplayableAddress() {
        return mDisplayableAddress;
    }

    public void setDisplayableAddress(String mDisplayableAddress) {
        this.mDisplayableAddress = mDisplayableAddress;
    }

    public String getAgencyContactPhoto() {
        return mAgencyContactPhoto;
    }

    public void setAgencyContactPhoto(String mAgencyContactPhoto) {
        this.mAgencyContactPhoto = mAgencyContactPhoto;
    }

    public String getTruncatedDescription() {
        return mTruncatedDescription;
    }

    public void setTruncatedDescription(String mTruncatedDescription) {
        this.mTruncatedDescription = mTruncatedDescription;
    }

    public String getRetinaDisplayThumbUrl() {
        return mRetinaDisplayThumbUrl;
    }

    public void setRetinaDisplayThumbUrl(String mRetinaDisplayThumbUrl) {
        this.mRetinaDisplayThumbUrl = mRetinaDisplayThumbUrl;
    }

    public String getSecondRetinaDisplayThumbUrl() {
        return mSecondRetinaDisplayThumbUrl;
    }

    public void setSecondRetinaDisplayThumbUrl(String mSecondRetinaDisplayThumbUrl) {
        this.mSecondRetinaDisplayThumbUrl = mSecondRetinaDisplayThumbUrl;
    }

    public ArrayList<String> getImageUrls() {
        return mImageUrls;
    }

    public void setImageUrls(ArrayList<String> mImageUrls) {
        this.mImageUrls = mImageUrls;
    }

    public ArrayList<Features> getFeatures() {
        return mFeatures;
    }

    public void setFeatures(ArrayList<Features> mFeatures) {
        this.mFeatures = mFeatures;
    }
}
