package com.domain.test.android.app.intf;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public interface IPullToRefresh {
    void onPullToRefresh();
}
