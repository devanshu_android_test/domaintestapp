package com.domain.test.android.app.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.domain.test.android.app.R;
import com.domain.test.android.app.base.AbstractBaseItemViewHolder;
import com.domain.test.android.app.model.ImageDimensions;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class PropertyViewHolder extends AbstractBaseItemViewHolder {

    public ImageView mShortlisted;

    public NetworkImageView mAgencyLogo;
    public NetworkImageView mPropertyImage1;
    public NetworkImageView mPropertyImage2;

    public View mAgencyColor;

    public TextView mPropertyPrice;
    public TextView mPropertyAddress;

    public TextView mTotalCarPark;
    public TextView mTotalBedrooms;
    public TextView mTotalBathrooms;

    public PropertyViewHolder(View mView, ImageDimensions mAgencyImageDimensions, ImageDimensions mThumbImageDimensions) {
        super(mView);

        mPropertyImage1 = mView.findViewById(R.id.display_thumb_image1);
        mPropertyImage2 = mView.findViewById(R.id.display_thumb_image2);

        mShortlisted = mView.findViewById(R.id.shortlisted);
        mAgencyColor = mView.findViewById(R.id.agent_color);

        mTotalCarPark   = mView.findViewById(R.id.car_park);
        mTotalBedrooms  = mView.findViewById(R.id.bedrooms);
        mTotalBathrooms = mView.findViewById(R.id.bathrooms);

        mAgencyLogo      = mView.findViewById(R.id.agency_logo);
        mPropertyPrice   = mView.findViewById(R.id.property_price);
        mPropertyAddress = mView.findViewById(R.id.property_address);

        mAgencyLogo.getLayoutParams().width  = mAgencyImageDimensions.getWidth();
        mAgencyLogo.getLayoutParams().height = mAgencyImageDimensions.getHeight();

        mPropertyImage1.getLayoutParams().width  = mThumbImageDimensions.getWidth();
        mPropertyImage1.getLayoutParams().height = mThumbImageDimensions.getHeight();

        mPropertyImage2.getLayoutParams().width  = mThumbImageDimensions.getWidth();
        mPropertyImage2.getLayoutParams().height = mThumbImageDimensions.getHeight();
    }
}
