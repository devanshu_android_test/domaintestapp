package com.domain.test.android.app.manager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class PreferenceManager {

    private static SharedPreferences sharedPrefMgr;
    private static SharedPreferences.Editor preferenceEditor;
    private static Context mContext;

    public static void createSharedPreferences(final Context mContext) {
        PreferenceManager.mContext = mContext;

        sharedPrefMgr    = android.preference.PreferenceManager.getDefaultSharedPreferences(PreferenceManager.mContext);
        preferenceEditor = sharedPrefMgr.edit();
    }

    public static synchronized void setBoolean(String mKey, boolean mValue) {
        if(null != preferenceEditor) {
            preferenceEditor.putBoolean(mKey, mValue);
            preferenceEditor.apply();
        }
    }

    public static Boolean getBoolean(String mKey) {
        if(null != preferenceEditor) {
            return sharedPrefMgr.getBoolean(mKey, false);
        } else {
            return false;
        }
    }
}
