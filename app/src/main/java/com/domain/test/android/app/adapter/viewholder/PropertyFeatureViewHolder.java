package com.domain.test.android.app.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.domain.test.android.app.R;
import com.domain.test.android.app.base.AbstractBaseItemViewHolder;

/**
 * Created by Devanshu.Verma on 31/1/18.
 */
public class PropertyFeatureViewHolder extends AbstractBaseItemViewHolder {

    public TextView mFeatureName;
    public TextView mFeatureValue;

    public PropertyFeatureViewHolder(View mView) {
        super(mView);

        mFeatureName  = mView.findViewById(R.id.feature_name);
        mFeatureValue = mView.findViewById(R.id.feature_value);
    }
}
