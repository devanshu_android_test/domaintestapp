package com.domain.test.android.app.contract.fragment;

import android.content.BroadcastReceiver;

import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.model.Listing;

/**
 * Created by Devanshu.Verma on 30/1/18.
 */

public interface IPropertyDetails {
    interface View extends IBaseView.View {
        void onSetLayoutWidth(View mView, int mScreenHeight, int mScreenWidth);
        void onUpdateView(View mView, Listing mProperty);
    }

    interface Presenter extends IBaseView.Presenter {
        void attachView(View mView);
        void setLayoutWidth();
        void dataReceived(Listing mProperty);
        void registerReceiver(String mFilter, BroadcastReceiver mReceiver);
        void unregisterReceiver(BroadcastReceiver mReceiver);

        String getDateFromString(String mDateTimeString);
    }
}
