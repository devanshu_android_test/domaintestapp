package com.domain.test.android.app.contract.view;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public interface IBaseView {
    interface View {
        void onInitialiseView(View mView);
    }

    interface Presenter {
    }
}
