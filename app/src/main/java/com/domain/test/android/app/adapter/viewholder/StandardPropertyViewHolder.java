package com.domain.test.android.app.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.domain.test.android.app.R;
import com.domain.test.android.app.base.AbstractBaseItemViewHolder;
import com.domain.test.android.app.model.ImageDimensions;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class StandardPropertyViewHolder extends AbstractBaseItemViewHolder {

    public View mAgencyColor;
    public ImageView mShortlisted;

    public NetworkImageView mAgencyLogo;
    public NetworkImageView mPropertyImage;

    public TextView mStatusLabel;
    public TextView mPropertyPrice;
    public TextView mPropertyAddress;

    public TextView mTotalCarPark;
    public TextView mTotalBedrooms;
    public TextView mTotalBathrooms;

    public StandardPropertyViewHolder(View mView, ImageDimensions mAgencyImageDimensions, ImageDimensions mThumbImageDimensions) {
        super(mView);

        mShortlisted = mView.findViewById(R.id.shortlisted);
        mAgencyColor = mView.findViewById(R.id.agent_color);

        mStatusLabel    = mView.findViewById(R.id.status_label);
        mTotalCarPark   = mView.findViewById(R.id.car_park);
        mTotalBedrooms  = mView.findViewById(R.id.bedrooms);
        mTotalBathrooms = mView.findViewById(R.id.bathrooms);

        mPropertyPrice   = mView.findViewById(R.id.property_price);
        mPropertyAddress = mView.findViewById(R.id.property_address);

        mAgencyLogo    = mView.findViewById(R.id.agency_logo);
        mPropertyImage = mView.findViewById(R.id.display_thumb_image);

        mAgencyLogo.getLayoutParams().width  = mAgencyImageDimensions.getWidth();
        mAgencyLogo.getLayoutParams().height = mAgencyImageDimensions.getHeight();

        mPropertyImage.getLayoutParams().width  = mThumbImageDimensions.getWidth();
        mPropertyImage.getLayoutParams().height = mThumbImageDimensions.getHeight();
    }
}
