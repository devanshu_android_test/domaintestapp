package com.domain.test.android.app.contract.activity;

import com.domain.test.android.app.contract.view.IBaseView;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public interface IDashboard {
    interface View extends IBaseView.View {
        void onLoadFragments(View nView, boolean isTablet);
        void onSetFragmentWidth(View nView, int mWidth);
    }

    interface Presenter {
        void attachView(View mView);
        void setDeviceType(boolean isTablet);
        void loadFragment();
        void setFragmentWidth();

        boolean isDeviceTablet();
    }
}
