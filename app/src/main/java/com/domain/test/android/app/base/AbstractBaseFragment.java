package com.domain.test.android.app.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.view.MotionEvent;
import android.view.View;

import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.intf.IItemClickListener;
import com.domain.test.android.app.intf.IPullToRefresh;
import com.domain.test.android.app.intf.IRequestListener;
import com.domain.test.android.app.logger.Logger;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public abstract class AbstractBaseFragment extends Fragment implements IBaseView.View, View.OnClickListener, View.OnTouchListener, IItemClickListener, IRequestListener {

    protected final String TAG = this.getClass().getSimpleName();

    protected View    mRootView;
    protected Context mContext;

    protected RecyclerView  mRecyclerListView;
    protected LayoutManager mRecyclerLayoutManager;
    protected SwipeRefreshLayout mSwipeContainer;

    protected IPullToRefresh mPullToRefresh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
    }

    @Override
    public void onClick(View mView) {
        Logger.debug(TAG, "Click on view :" + mView.getClass().getSimpleName());
    }

    @Override
    public boolean onTouch(View mView, MotionEvent mMotionEvent) {
        return true;
    }

    @Override
    public void onItemClick(View mView, Object mTagObject) {
        Logger.debug(TAG, "Click on item :" + mView.getClass().getSimpleName());
    }

    @Override
    public void onError(Object mErrorResponse) {
        Logger.debug(TAG, "API response error");
    }

    @Override
    public void onSuccess(Object mSuccessResponse) {
        Logger.debug(TAG, "API response successful");
    }

    protected void showProgressBar(int mProgressBarId) {
        this.mRootView.findViewById(mProgressBarId).setVisibility(View.VISIBLE);
    }

    protected void stopProgressBar(int mProgressBarId) {
        this.mRootView.findViewById(mProgressBarId).setVisibility(View.GONE);
    }

    protected SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        public void onRefresh() {
            if(null != AbstractBaseFragment.this.mPullToRefresh) {
                AbstractBaseFragment.this.mPullToRefresh.onPullToRefresh();
            }

        }
    };
}
