package com.domain.test.android.app.manager;

import com.domain.test.android.app.common.UrlBuilder;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class RequestManager {

    public static String getSearchRequest(String mSub, String mMode, String mState, String mPCode) {
        UrlBuilder mBuilder = new UrlBuilder(UrlManager.MapSearch.EndPoint);
        mBuilder.addParameter("sub", mSub).addParameter("mode", mMode).addParameter("state", mState).addParameter("pcodes", mPCode);
        return mBuilder.build();
    }
}
