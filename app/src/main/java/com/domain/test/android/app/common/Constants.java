package com.domain.test.android.app.common;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class Constants {

    public static class AspectRatio {
        public static class ThumbImage {
            public static final int Width  = 16;
            public static final int Height = 10;

            public static final float ImageOccupancy = .46f;
        }

        public static class AgencyLogo {
            public static final int Width  = 21;
            public static final int Height = 9;

            public static final float ImageOccupancy = .15f;
        }

        public static class HeroPager {
            public static final int Width  = 16;
            public static final int Height = 10;

            public static final float ImageOccupancy = 1.0f;
        }

        public static class AgencyContact {
            public static final int Width  = 4;
            public static final int Height = 3;

            public static final float ImageOccupancy = .20f;
        }
    }

    public static class Broadcast {
        public static final String ListingDetails  = "com.domain.test.android.app.common.broadcast.ListingDetails";
    }

    public static class IntentKey {
        public static final String Data = "Data";
    }
}
