package com.domain.test.android.app.application;

import android.app.Application;

import com.domain.test.android.app.logger.Logger;
import com.domain.test.android.app.manager.PreferenceManager;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class DomainTestApp extends Application {

    private final String TAG = DomainTestApp.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        PreferenceManager.createSharedPreferences(this);
        Logger.debug(TAG, "Application class created");
    }
}
