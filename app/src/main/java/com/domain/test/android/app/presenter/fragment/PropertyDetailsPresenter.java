package com.domain.test.android.app.presenter.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;

import com.domain.test.android.app.base.AbstractBasePresenter;
import com.domain.test.android.app.common.StorageKey;
import com.domain.test.android.app.common.Utility;
import com.domain.test.android.app.contract.fragment.IPropertyDetails;
import com.domain.test.android.app.contract.view.IBaseView;
import com.domain.test.android.app.controller.broadcast.BroadcastController;
import com.domain.test.android.app.manager.CalendarManager;
import com.domain.test.android.app.manager.PreferenceManager;
import com.domain.test.android.app.model.Listing;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Devanshu.Verma on 30/1/18.
 */
public class PropertyDetailsPresenter  extends AbstractBasePresenter<IPropertyDetails.View> implements IPropertyDetails.Presenter {

    public PropertyDetailsPresenter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public IPropertyDetails.View getGhostView() {
        return new IPropertyDetails.View() {
            @Override
            public void onSetLayoutWidth(IPropertyDetails.View mView, int mScreenHeight, int mScreenWidth) {
            }

            @Override
            public void onUpdateView(IPropertyDetails.View mView, Listing mProperty) {
            }

            @Override
            public void onInitialiseView(IBaseView.View mView) {
            }
        };
    }

    @Override
    public void attachView(IPropertyDetails.View mView) {
        super.attachView(mView);
        getView().onInitialiseView(mView);
        setLayoutWidth();
    }

    @Override
    public void setLayoutWidth() {
        int mScreenWidth  = PreferenceManager.getBoolean(StorageKey.TabletDevice) ? Utility.getDeviceWidth(mContext)/2: Utility.getDeviceWidth(mContext);
        int mScreenHeight = Utility.getDeviceHeight(mContext);
        getView().onSetLayoutWidth(getView(), mScreenHeight, mScreenWidth);
    }

    @Override
    public void dataReceived(Listing mProperty) {
        if(null != mProperty){
            getView().onUpdateView(getView(), mProperty);
        }
    }

    @Override
    public void registerReceiver(String mFilter, BroadcastReceiver mReceiver) {
        BroadcastController.registerReceiver(mContext, mFilter, mReceiver);
    }

    @Override
    public void unregisterReceiver(BroadcastReceiver mReceiver) {
        BroadcastController.unregisterReceiver(mContext, mReceiver);
    }

    @Override
    public String getDateFromString(String mDateTimeString) {
        String mDateTime;
        DateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date mFormattedDate = mSimpleDateFormat.parse(mDateTimeString);
            Calendar mCalendar = Calendar.getInstance();
            mCalendar.setTimeInMillis(mFormattedDate.getTime());
            mDateTime = CalendarManager.getDate(mCalendar);
        } catch (ParseException mException) {
            mException.printStackTrace();
            mDateTime = "";
        }

        return mDateTime;
    }
}
