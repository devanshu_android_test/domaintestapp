package com.domain.test.android.app.logger;

import android.util.Log;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class Logger {

    public static void info(String mTag, String mLogMsg) {
        Log.i(mTag, mLogMsg);
    }

    public static void error(String mTag, String mLogMsg) {
        Log.e(mTag, mLogMsg);
    }

    public static void error(String mTag, String mLogMsg, Exception mException) {
        Log.e(mTag, mLogMsg +"\n Exception: " + mException.getLocalizedMessage());
    }

    public static void debug(String mTag, String mLogMsg) {
        Log.d(mTag, mLogMsg);
    }

    public static void verbose(String mTag, String mLogMsg) {
        Log.v(mTag, mLogMsg);
    }

    public static void warning(String mTag, String mLogMsg) {
        Log.w(mTag, mLogMsg);
    }

    public static void wtf(String mTag, String mLogMsg) {
        Log.wtf(mTag, mLogMsg);
    }
}
