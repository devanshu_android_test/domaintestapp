package com.domain.test.android.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.domain.test.android.app.R;
import com.domain.test.android.app.adapter.viewholder.PremiumPropertyViewHolder;
import com.domain.test.android.app.adapter.viewholder.PropertyViewHolder;
import com.domain.test.android.app.adapter.viewholder.StandardPropertyViewHolder;
import com.domain.test.android.app.base.AbstractBaseItemViewHolder;
import com.domain.test.android.app.base.AbstractBaseRecyclerListAdapter;
import com.domain.test.android.app.common.Constants;
import com.domain.test.android.app.common.StorageKey;
import com.domain.test.android.app.common.Utility;
import com.domain.test.android.app.controller.api.VolleyRequestHelper;
import com.domain.test.android.app.manager.PreferenceManager;
import com.domain.test.android.app.model.ImageDimensions;
import com.domain.test.android.app.model.Listing;

import java.util.ArrayList;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public class PropertyListAdapter extends AbstractBaseRecyclerListAdapter<PropertyViewHolder> {

    private ImageDimensions mThumbImageDimension;
    private ImageDimensions mAgencyImageDimensions;

    private ArrayList<Listing> mPropertyListing;

    public PropertyListAdapter(Context mContext, RecyclerView mRecyclerView, ArrayList<Listing> mPropertyListing) {
        this.mContext         = mContext;
        this.mImageLoader     = VolleyRequestHelper.getInstance(mContext).getImageLoader();
        this.mRecyclerView    = mRecyclerView;
        this.mPropertyListing = mPropertyListing;

        if(null != mRecyclerView) {
            mRecyclerView.addOnScrollListener(mScrollListener);
        }

        if(PreferenceManager.getBoolean(StorageKey.TabletDevice)) {
            mThumbImageDimension   = Utility.getImageDimensions((Utility.getDeviceWidth(mContext)/2), Constants.AspectRatio.ThumbImage.Width, Constants.AspectRatio.ThumbImage.Height, Constants.AspectRatio.ThumbImage.ImageOccupancy);
            mAgencyImageDimensions = Utility.getImageDimensions((Utility.getDeviceWidth(mContext)/2),Constants.AspectRatio.AgencyLogo.Width, Constants.AspectRatio.AgencyLogo.Height, Constants.AspectRatio.AgencyLogo.ImageOccupancy);
        } else {
            mThumbImageDimension   = Utility.getImageDimensions(mContext, Constants.AspectRatio.ThumbImage.Width, Constants.AspectRatio.ThumbImage.Height, Constants.AspectRatio.ThumbImage.ImageOccupancy);
            mAgencyImageDimensions = Utility.getImageDimensions(mContext, Constants.AspectRatio.AgencyLogo.Width, Constants.AspectRatio.AgencyLogo.Height, Constants.AspectRatio.AgencyLogo.ImageOccupancy);
        }
        this.isScrolling = true;
    }

    @Override
    public Listing getItem(int mPosition) {
        return null != mPropertyListing ? mPropertyListing.get(mPosition) : null;
    }

    @Override
    public int getItemCount() {
        return null != mPropertyListing ? mPropertyListing.size() : 0;
    }

    @Override
    public int getItemViewType(int mPosition) {
        return mPropertyListing.get(mPosition).getIsElite();
    }

    @Override
    public synchronized void updateAdapterList(Object mAdapterList) {
        mPropertyListing = (ArrayList<Listing>) mAdapterList;
        notifyDataSetChanged();
    }

    @Override
    public PropertyViewHolder getViewHolder(ViewGroup mParent, int mViewType) {
        /*AbstractBaseItemViewHolder mViewHolder;
        View mView;
        if(ViewPremium == mViewType) {
            mView = LayoutInflater.from(mParent.getContext()).inflate(
                    R.layout.list_item_premium_feed, mParent, false);
            mViewHolder = new PremiumPropertyViewHolder(mView, mAgencyImageDimensions, mThumbImageDimension);
        } else {
            mView = LayoutInflater.from(mParent.getContext()).inflate(
                    R.layout.list_item_standard_feed, mParent, false);
            mViewHolder = new StandardPropertyViewHolder(mView, mAgencyImageDimensions, mThumbImageDimension);
        }*/
        PropertyViewHolder mViewHolder;
        View mView;
        if(ViewPremium == mViewType) {
            mView = LayoutInflater.from(mParent.getContext()).inflate(
                    R.layout.list_item_premium_feed, mParent, false);
        } else {
            mView = LayoutInflater.from(mParent.getContext()).inflate(
                    R.layout.list_item_standard_feed, mParent, false);
        }
        mViewHolder = new PropertyViewHolder(mView, mAgencyImageDimensions, mThumbImageDimension);
        return mViewHolder;
    }

    @Override
    public PropertyViewHolder onCreateViewHolder(ViewGroup mParent, int mViewType) {
        return getViewHolder(mParent, mViewType);
    }

    @Override
    public void onBindViewHolder(PropertyViewHolder mViewHolder, int mPosition) {
        /*if(mViewHolder instanceof StandardPropertyViewHolder) {
            ((StandardPropertyViewHolder) mViewHolder).mAgencyColor.setBackgroundColor(Color.parseColor(mPropertyListing.get(mPosition).getAgencyColour()));
            ((StandardPropertyViewHolder) mViewHolder).mTotalCarPark.setText(mPropertyListing.get(mPosition).getCarSpaces() + mContext.getResources().getString(R.string.car_park));
            ((StandardPropertyViewHolder) mViewHolder).mPropertyPrice.setText(mPropertyListing.get(mPosition).getDisplayPrice());
            ((StandardPropertyViewHolder) mViewHolder).mTotalBedrooms.setText(mPropertyListing.get(mPosition).getBedrooms() + mContext.getResources().getString(R.string.bedroom));
            ((StandardPropertyViewHolder) mViewHolder).mTotalBathrooms.setText(mPropertyListing.get(mPosition).getBathrooms() + mContext.getResources().getString(R.string.bathroom));
            if(null != mPropertyListing.get(mPosition).getStatusLabel() && !TextUtils.isEmpty(mPropertyListing.get(mPosition).getStatusLabel()))
                ((StandardPropertyViewHolder) mViewHolder).mStatusLabel.setText(mPropertyListing.get(mPosition).getStatusLabel());
            else
                ((StandardPropertyViewHolder) mViewHolder).mStatusLabel.setVisibility(View.INVISIBLE);
            ((StandardPropertyViewHolder) mViewHolder).mPropertyAddress.setText(mPropertyListing.get(mPosition).getDisplayableAddress());
            ((StandardPropertyViewHolder) mViewHolder).mAgencyLogo.setImageUrl(mPropertyListing.get(mPosition).getAgencyLogoUrl(), mImageLoader);
            ((StandardPropertyViewHolder) mViewHolder).mPropertyImage.setImageUrl(mPropertyListing.get(mPosition).getRetinaDisplayThumbUrl(), mImageLoader);

            if(mPropertyListing.get(mPosition).isShortlisted())
                ((StandardPropertyViewHolder) mViewHolder).mShortlisted.setImageResource(android.R.drawable.star_big_on);
            else
                ((StandardPropertyViewHolder) mViewHolder).mShortlisted.setImageResource(android.R.drawable.star_big_off);

            ((StandardPropertyViewHolder) mViewHolder).mShortlisted.setTag(mPosition);
            ((StandardPropertyViewHolder) mViewHolder).mShortlisted.setOnClickListener(this);

            ((StandardPropertyViewHolder) mViewHolder).itemView.setTag(mPosition);
            ((StandardPropertyViewHolder) mViewHolder).itemView.setOnClickListener(this);
        } else if(mViewHolder instanceof PremiumPropertyViewHolder) {
            ((PremiumPropertyViewHolder) mViewHolder).mPropertyImage1.setImageUrl(mPropertyListing.get(mPosition).getRetinaDisplayThumbUrl(), mImageLoader);
            ((PremiumPropertyViewHolder) mViewHolder).mPropertyImage2.setImageUrl(mPropertyListing.get(mPosition).getSecondRetinaDisplayThumbUrl(), mImageLoader);
            ((PremiumPropertyViewHolder) mViewHolder).mAgencyColor.setBackgroundColor(Color.parseColor(mPropertyListing.get(mPosition).getAgencyColour()));
            ((PremiumPropertyViewHolder) mViewHolder).mTotalCarPark.setText(mPropertyListing.get(mPosition).getCarSpaces() + mContext.getResources().getString(R.string.car_park));
            ((PremiumPropertyViewHolder) mViewHolder).mPropertyPrice.setText(mPropertyListing.get(mPosition).getDisplayPrice());
            ((PremiumPropertyViewHolder) mViewHolder).mTotalBedrooms.setText(mPropertyListing.get(mPosition).getBedrooms() + mContext.getResources().getString(R.string.bedroom));
            ((PremiumPropertyViewHolder) mViewHolder).mTotalBathrooms.setText(mPropertyListing.get(mPosition).getBathrooms() + mContext.getResources().getString(R.string.bathroom));
            ((PremiumPropertyViewHolder) mViewHolder).mPropertyAddress.setText(mPropertyListing.get(mPosition).getDisplayableAddress());
            ((PremiumPropertyViewHolder) mViewHolder).mAgencyLogo.setImageUrl(mPropertyListing.get(mPosition).getAgencyLogoUrl(), mImageLoader);

            if(mPropertyListing.get(mPosition).isShortlisted())
                ((PremiumPropertyViewHolder) mViewHolder).mShortlisted.setImageResource(android.R.drawable.star_big_on);
            else
                ((PremiumPropertyViewHolder) mViewHolder).mShortlisted.setImageResource(android.R.drawable.star_big_off);

            ((PremiumPropertyViewHolder) mViewHolder).mShortlisted.setTag(mPosition);
            ((PremiumPropertyViewHolder) mViewHolder).mShortlisted.setOnClickListener(this);

            ((PremiumPropertyViewHolder) mViewHolder).itemView.setTag(mPosition);
            ((PremiumPropertyViewHolder) mViewHolder).itemView.setOnClickListener(this);
        }*/
        mViewHolder.mPropertyImage1.setImageUrl(mPropertyListing.get(mPosition).getRetinaDisplayThumbUrl(), mImageLoader);
        if(null != mViewHolder.mPropertyImage2)
            mViewHolder.mPropertyImage2.setImageUrl(mPropertyListing.get(mPosition).getSecondRetinaDisplayThumbUrl(), mImageLoader);
        mViewHolder.mAgencyColor.setBackgroundColor(Color.parseColor(mPropertyListing.get(mPosition).getAgencyColour()));
        mViewHolder.mTotalCarPark.setText(mPropertyListing.get(mPosition).getCarSpaces() + mContext.getResources().getString(R.string.car_park));
        mViewHolder.mPropertyPrice.setText(mPropertyListing.get(mPosition).getDisplayPrice());
        mViewHolder.mTotalBedrooms.setText(mPropertyListing.get(mPosition).getBedrooms() + mContext.getResources().getString(R.string.bedroom));
        mViewHolder.mTotalBathrooms.setText(mPropertyListing.get(mPosition).getBathrooms() + mContext.getResources().getString(R.string.bathroom));
        mViewHolder.mPropertyAddress.setText(mPropertyListing.get(mPosition).getDisplayableAddress());
        mViewHolder.mAgencyLogo.setImageUrl(mPropertyListing.get(mPosition).getAgencyLogoUrl(), mImageLoader);

        if(mPropertyListing.get(mPosition).isShortlisted())
            mViewHolder.mShortlisted.setImageResource(android.R.drawable.star_big_on);
        else
            mViewHolder.mShortlisted.setImageResource(android.R.drawable.star_big_off);

        mViewHolder.mShortlisted.setTag(mPosition);
        mViewHolder.mShortlisted.setOnClickListener(this);

        mViewHolder.itemView.setTag(mPosition);
        mViewHolder.itemView.setOnClickListener(this);
        if(isScrolling)
            mViewHolder.itemView.setAnimation(AnimationUtils.loadAnimation(this.mContext, R.anim.appear));
    }
}
