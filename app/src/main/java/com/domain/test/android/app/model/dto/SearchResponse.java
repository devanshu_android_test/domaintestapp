package com.domain.test.android.app.model.dto;

import com.domain.test.android.app.model.ListingResult;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public class SearchResponse {

    @SerializedName("ListingResults")
    private ListingResult mListingResults;

    public ListingResult getListingResults() {
        return mListingResults;
    }

    public void setListingResults(ListingResult mListingResults) {
        this.mListingResults = mListingResults;
    }
}
