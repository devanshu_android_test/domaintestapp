package com.domain.test.android.app.base;

import android.content.Context;

import java.lang.ref.WeakReference;

/**
 * Created by Devanshu.Verma on 24/1/18.
 */
public abstract class AbstractBasePresenter <View> {

    protected final String TAG = this.getClass().getSimpleName();

    private View mGhostView;
    private WeakReference<View> mReference;

    protected Context mContext;

    public void attachView(View mView) {
        mReference = new WeakReference<>(mView);
    }

    public View getView() {
        View mView = (mReference != null ? mReference.get() : null);
        if (mView == null) {
            if (mGhostView == null) {
                mGhostView = getGhostView();
            }
            mView = mGhostView;
        }
        return mView;
    }

    public abstract View getGhostView();
}