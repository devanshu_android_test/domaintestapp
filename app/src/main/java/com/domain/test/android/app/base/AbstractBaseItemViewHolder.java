package com.domain.test.android.app.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Devanshu.Verma on 25/1/18.
 */
public abstract class AbstractBaseItemViewHolder extends RecyclerView.ViewHolder {
    public AbstractBaseItemViewHolder(View mView){
        super(mView);
    }
}
