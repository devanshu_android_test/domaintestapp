package com.domain.test.android.app.manager;

import java.util.Calendar;

/**
 * Created by Devanshu.Verma on 31/1/18.
 */
public class CalendarManager {

    public static final String[] Months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"};

    public static String getDate(Calendar mCalendar) {
        return String.format("%02d%s %s %04d", mCalendar.get(Calendar.DAY_OF_MONTH),
                getPostFix(mCalendar.get(Calendar.DAY_OF_MONTH)), CalendarManager.Months[mCalendar.get(Calendar.MONTH)], mCalendar.get(Calendar.YEAR));
    }

    private static String getPostFix(int mDate) {
        if(mDate == 1 || mDate == 21 || mDate == 31)
            return "st";
        else
        if(mDate == 2 || mDate == 22 )
            return "nd";
        else
        if(mDate == 3 || mDate == 23)
            return "rd";
        else
            return "th";
    }

}
