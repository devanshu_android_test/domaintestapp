package com.domain.test.android.app.common;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

import com.domain.test.android.app.model.ImageDimensions;

/**
 * Created by Devanshu.Verma on 29/1/18.
 */
public class Utility {

    public static int getDeviceWidth(Context mContext) {
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        ((Activity)mContext).getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.widthPixels;
    }

    public static int getDeviceHeight(Context mContext) {
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        ((Activity)mContext).getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        return mDisplayMetrics.heightPixels;
    }

    public static ImageDimensions getImageDimensions(Context mContext, final int mWidthAspect, final int mHeightAspect, final float mImageOccupancy) {
        return getImageDimensions(getDeviceWidth(mContext), mWidthAspect, mHeightAspect, mImageOccupancy);
    }

    /**
     * Get width and height for image container based on screen size, aspect ratio and occupancy percentage.
     *
     * @param mScreenWidth
     * @param mWidthAspect
     * @param mHeightAspect
     * @param mImageOccupancy
     *
     * @return ImageDimensions
     *
     * */
    public static ImageDimensions getImageDimensions(int mScreenWidth, final int mWidthAspect, final int mHeightAspect, final float mImageOccupancy) {
        int mWidth  = (int)(mScreenWidth * mImageOccupancy);
        int mHeight = ((mWidth * mHeightAspect) / mWidthAspect);

        return new ImageDimensions(mWidth, mHeight);
    }
}
