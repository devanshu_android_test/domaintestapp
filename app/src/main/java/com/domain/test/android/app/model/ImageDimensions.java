package com.domain.test.android.app.model;

import java.io.Serializable;

/**
 * Created by Devanshu.Verma on 29/1/18.
 */
public class ImageDimensions implements Serializable {

    private int mWidth;
    private int mHeight;

    public ImageDimensions() {
    }

    public ImageDimensions(int mWidth, int mHeight) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int mHeight) {
        this.mHeight = mHeight;
    }
}
