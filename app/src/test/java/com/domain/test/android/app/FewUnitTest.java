package com.domain.test.android.app;

import com.domain.test.android.app.manager.CalendarManager;
import com.domain.test.android.app.manager.RequestManager;
import com.domain.test.android.app.manager.UrlManager;

import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

public class FewUnitTest {

    @Test
    public void testRequestManagerGetSearchRequest() {
        final String mTestUrl = UrlManager.MapSearch.EndPoint + "?mode=buy&sub=Bondi&pcodes=2026&state=NSW";
        assertEquals(mTestUrl, RequestManager.getSearchRequest("Bondi", "buy", "NSW", "2026"));
    }

    @Test
    public void testCalendarManagerGetDate() {
        final String mTestDate = "31st Jan 2018";
        Calendar mCalendar = Calendar.getInstance();
        mCalendar.setTimeInMillis(System.currentTimeMillis());
        mCalendar.set(Calendar.DAY_OF_MONTH, 31);
        mCalendar.set(Calendar.MONTH, Calendar.JANUARY);
        mCalendar.set(Calendar.YEAR, 2018);

        assertEquals(mTestDate, CalendarManager.getDate(mCalendar));
    }
}