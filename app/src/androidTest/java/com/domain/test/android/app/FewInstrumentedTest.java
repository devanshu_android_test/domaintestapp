package com.domain.test.android.app;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.domain.test.android.app.common.Constants;
import com.domain.test.android.app.common.Utility;
import com.domain.test.android.app.controller.device.DeviceController;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class FewInstrumentedTest {

    @Test
    public void testInternetConnected() {
        assertEquals(true, DeviceController.isInternetAvailable(InstrumentationRegistry.getTargetContext()));
    }

    @Test
    public void testAppPackageName() {
        assertEquals("com.domain.test.android.app", InstrumentationRegistry.getTargetContext().getPackageName());
    }

    @Test
    public void testGetDeviceWidth() {
        assert (Utility.getDeviceWidth(InstrumentationRegistry.getTargetContext()) > 0);
    }

    @Test
    public void testGetDeviceHeight() {
        assert (Utility.getDeviceHeight(InstrumentationRegistry.getTargetContext()) > 0);
    }

    @Test
    public void testGetImageDimensions1() {
        assert (Utility.getImageDimensions(InstrumentationRegistry.getTargetContext(), Constants.AspectRatio.ThumbImage.Width, Constants.AspectRatio.ThumbImage.Height, Constants.AspectRatio.ThumbImage.ImageOccupancy) != null);
    }

    @Test
    public void testGetImageDimensions2() {
        assert (Utility.getImageDimensions(Utility.getDeviceWidth(InstrumentationRegistry.getTargetContext()), Constants.AspectRatio.ThumbImage.Width, Constants.AspectRatio.ThumbImage.Height, Constants.AspectRatio.ThumbImage.ImageOccupancy) != null);
    }
}
